# base image
FROM alpine:3

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Florian Schwab <me@ydkn.io>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="ydkn/avahi" \
  org.label-schema.description="Simple Avahi docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/r/ydkn/avahi" \
  org.label-schema.vcs-url="https://gitlab.com/ydkn/docker-avahi" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN apk --no-cache --no-progress add avahi avahi-tools

# remove default services
RUN rm /etc/avahi/services/*

# disable d-bus
RUN sed -i 's/.*enable-dbus=.*/enable-dbus=no/' /etc/avahi/avahi-daemon.conf

# entrypoint
ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT [ "docker-entrypoint.sh" ]

# default command
CMD ["avahi-daemon"]

# volumes
VOLUME ["/etc/avahi/services"]
