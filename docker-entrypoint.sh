#!/bin/sh -e

if [ -n "${ALLOW_INTERFACES}" ]; then
  sed -i "s/.*allow-interfaces=.*/allow-interfaces=${ALLOW_INTERFACES}/" /etc/avahi/avahi-daemon.conf
fi

if [ -n "${DENY_INTERFACES}" ]; then
  sed -i "s/.*deny-interfaces=.*/deny-interfaces=${DENY_INTERFACES}/" /etc/avahi/avahi-daemon.conf
fi

if [ "${ENABLE_REFLECTOR}" = "yes" ]; then
  sed -i "s/.*enable-reflector=.*/enable-reflector=yes/" /etc/avahi/avahi-daemon.conf
fi

rm -f /var/run/avahi-daemon/*

exec "$@"
